# PARTIE 2 - Modifier une entité User et templating

## 2.1 - Et si on veut modifier les entités après coup ?

Oups, nous avons oublié le champ Password dans l'entité User. Heureusement, il est possible d'y remédier. Nous allons ajouter ce champ au modèle User, puis mettre à jour la base de donnée.

+ Ajouter le champs Password au modèle User (cf. doc Buffalo)
+ Utiliser Fizz avec buffalo en ligne de commande pour générer une migration (voir $ buffalo db g -h)

2 fichiers sont générés sous migrations/ :
- migrations/aaaammjj_add_password.up.fizz : pour ajouter une colonne à la table
- migrations/aaaammjj_add_password.down.fizz : pour supprimer une colonne de la table

Dans le fichier *up.fizz, écrire l'instruction permettant de créer une colonne pour le Password.
Dans le fichier *down.fizz, écrire l'instruction permettant de supprimer la colonne password (car ne peut être nulle).

Ensuite, appliquer la migration en utilisant l'outil en ligne de commande buffalo.

**Rappel:** pour vérifier le statut de votre migration, `$ buffalo db migrate status`


## 2.2 - Création d'un nouvel utilisateur

Dans templates/index.html, remplacer tout le contenu par :

```
<div class="row mt-5">
  <div class="col text-center">
    <h1>Welcome to my App!</h1>
  </div>
</div>
```

Dans /templates/application.html, remplacer `<div class="container></div>` par le bloc suivant :
```
    <div class="container">

      <!-- NavBar -->
      <nav class="navbar navbar-expand-lg navbar-light bg-light mb-2">

        <!--rootPath() est le nom de la route vers la racine de l'application-->
        <a class="navbar-brand" href="<%= rootPath() %>">My App</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item">
              <a class="nav-link" href="#">Posts</a>
            </li>
          </ul>
          <ul class="navbar-nav">

            <!--TODO : rediriger vers la création d'utilisateur -->
            <li class="nav-item">
              <a href="#" class="nav-link">Register</a>
            </li>

          </ul>
        </div>
      </nav>

      <%= partial("flash.html") %>
      <%= yield %>
    </div>
```

Pour l'instant, notre bouton "Register" ne renvoie nulle part. Il faut donc qu'il redirige vers le formulaire de création d'un nouvel utilisateur. Pour ce faire, modifiez le fichier `templates/application.html`.

**Rappel** : `$ buffalo routes` pour afficher toutes les routes créées dans notre app.

On voudrait maintenant qu'une fois enregistré, l'utilisateur soit redirigé vers la page d'accueil de l'application. Pour cela, modifiez le controller `actions/users.go`.

Maintenant vous devez pouvoir vous enregistrer et être redirigés vers la page d'accueil. Vous pouvez également vérifier que l'utilisateur a bien été créé sur l'url `http://localhost:3000/users/` .



